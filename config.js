module.exports = {
    'name'  : 'Features 99',
    'camel' : 'Features99',
    'slug'  : 'features99',
    'dob'   : 'features_99_1440',
    'desc'  : 'A main content col with supplemental content boxes',
}