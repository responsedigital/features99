<!-- Start Features 99 -->
@if(!Fir\Utils\Helpers::isProduction())
<!-- Description : A main content col with supplemental content boxes -->
@endif
<div class="features99 {{ $classes }}"  is="fir-features99">
  <script type="application/json">
      {
          "options": @json($jsonData)
      }
  </script>
  <div class="features99__wrap">
      <div class="features99__copy">
        <h2 class="features99__title">
            New best experience
        </h2>
        <p class="features99__text">
            Nam natum volutpat eu. Natum elitr vel te. Id qui purto dicit, bonorum minimum et sit. Cum ei assum tation homero, at per assum dicit verterem. Lorem deterruisset.
        </p>
        <a href="/" class="btn btn--hollow"> Explore</a>
      </div>

      <div class="features99__panels">
          <div class="features99__panel features99__panel--primary">
            <img src="https://res.cloudinary.com/fir-design/image/upload/v1591065565/Icons/firo-icon_icncqg.svg" class="features99__panel-icon">
            <h4 class="features99__title">Layouts</h4>
            <p class="features99__text">Quidam officiis similique sea ei, vel tollit indoctum efficiendi tantas platonem. </p>
          </div>

          <div class="features99__panel">
            <img src="https://res.cloudinary.com/fir-design/image/upload/v1591065565/Icons/firo-icon_icncqg.svg" class="features99__panel-icon">
            <h4 class="features99__title">Layouts</h4>
            <p class="features99__text">Quidam officiis similique sea ei, vel tollit indoctum efficiendi tantas platonem. </p>
          </div>

          <div class="features99__panel">
            <img src="https://res.cloudinary.com/fir-design/image/upload/v1591065565/Icons/firo-icon_icncqg.svg" class="features99__panel-icon">
            <h4 class="features99__title">Layouts</h4>
            <p class="features99__text">Quidam officiis similique sea ei, vel tollit indoctum efficiendi tantas platonem. </p>
          </div>

          <div class="features99__panel features99__panel--primary">
            <img src="https://res.cloudinary.com/fir-design/image/upload/v1591065565/Icons/firo-icon_icncqg.svg" class="features99__panel-icon">
            <h4 class="features99__title">Layouts</h4>
            <p class="features99__text">Quidam officiis similique sea ei, vel tollit indoctum efficiendi tantas platonem. </p>
          </div>
    </div>
  </div>
</div>
<!-- End Features 99 -->